﻿using Drow.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Drow.Base
{
    public class DrowDirector : AppDirector 
    {
        protected override void Awake()
        {
            base.Awake();

            SceneManager.LoadScene(ScenesIds.Menu);
        }
    }
}

