﻿using Drow.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow.Base
{
    
    public class GameInfo : BaseManager<GameInfo>
    {
        public UserData userData;
        public void Setup()
        {
            if (AppPrefs.HasKey(Ids.UserData))
            {
                userData = AppPrefs.GetObject<UserData>(Ids.UserData);
            }
            else
            {
                userData = new UserData();

                AppPrefs.SetObject(Ids.UserData, userData);
            }
        }
        public void RegisterResult(int _score)
        {
            if(InGameScores > BestScores)
            {
                BestScores = InGameScores;
            }

            AppPrefs.SetObject(Ids.UserData, userData);
            AppPrefs.Save();
        }
        
        public void RegisterScores()
        {
            InGameScores += Ids.SCORES_FOR_KIGHT;
        }
        public int BestScores { get => userData.BestScore; set => userData.BestScore = value; }
        public int InGameScores { get; set; }
    }
}

