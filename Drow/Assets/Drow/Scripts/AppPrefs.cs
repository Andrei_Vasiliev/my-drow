﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow
{
    public static class AppPrefs
    {
        public static void SetObject(string _key, object _obj)
        {
            SetString(_key, JsonUtility.ToJson(_obj));
        }

        public static T GetObject<T>(string _key)
        {
            return JsonUtility.FromJson<T>(GetString(_key));
        }

        public static void SetBool(string _key, bool _value)
        {
            PlayerPrefs.SetInt(_key, _value ? 1 : 0);
        }

        public static bool GetBool(string _key)
        {
            return PlayerPrefs.GetInt(_key) == 1;
        }

        public static void SetInt(string _key, int _value)
        {
            PlayerPrefs.SetInt(_key, _value);
        }

        public static int GetInt(string _key)
        {
            return PlayerPrefs.GetInt(_key);
        }

        public static void SetString(string _key, string _value)
        {
            PlayerPrefs.SetString(_key, _value);
        }

        public static string GetString(string _key)
        {
            return PlayerPrefs.GetString(_key);
        }

        public static bool HasKey(string _key)
        {
            return PlayerPrefs.HasKey(_key);
        }

        public static void Save()
        {
            PlayerPrefs.Save();
        }
    }
}