﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contein : MonoBehaviour
{
    public GameObject[] spells;
    public GameObject[] emptySpells;

    public Sprite[] fullLive;
    public Sprite[] emptyLive;
    private void Awake()
    {
        for (int i = 0; i < spells.Length; i++)
        {
            fullLive[i] = spells[i].GetComponent<SpriteRenderer>().sprite;
            emptyLive[i] = emptySpells[i].GetComponent<SpriteRenderer>().sprite;
        }
    }
}
