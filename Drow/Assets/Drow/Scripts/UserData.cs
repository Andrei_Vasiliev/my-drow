﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow
{
    [Serializable]
    public class UserData 
    {
        [SerializeField]
        int BestScores;
        [SerializeField]
        bool isSoundOn;
        [SerializeField]
        bool isMusicOn;

        public int BestScore { get => BestScores; set => BestScores = value; }
        public bool IsSoundOn { get => isSoundOn; set => isSoundOn = value; }
        public bool IsMusicOn { get => isMusicOn; set => isMusicOn = value; }
    }
}

