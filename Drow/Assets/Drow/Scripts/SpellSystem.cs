﻿using Drow;
using Drow.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellSystem : MonoBehaviour
{
    [SerializeField]
    KnightsConfig knightsConfig;

    public Transform parent;

    GameObject gameController;

    public Sprite[] live;
    public Sprite[] emptyLive;

    public List<Image> lives;
    public List<string> nameSpell;

    public bool curr = false;

    public int iterator = 0;

    private void Awake()
    {
        gameController = GameObject.Find("GameController");
        live = gameController.GetComponent<Contein>().fullLive;
        emptyLive = gameController.GetComponent<Contein>().emptyLive;

        for (int i = 1; i <= 4; i++)
        {
            lives[lives.Count - i].gameObject.SetActive(false);
        }
        FillSpell(knightsConfig.KnightConfigs[0].Lives);
    }
    private void Update()
    {
        if(GameInfo.Instance.InGameScores > 50 && nameSpell.Count < knightsConfig.KnightConfigs[1].Lives)
        {
            for (int i = 1; i <= 4; i++)
            {
                lives[lives.Count - i].gameObject.SetActive(true);
            }
            for (int i = 1; i <= 2 ; i++)
            {
                lives[lives.Count - i].gameObject.SetActive(false);
            }

            FillSpell(knightsConfig.KnightConfigs[1].Lives);
        }
        else if(GameInfo.Instance.InGameScores > 150 && nameSpell.Count < knightsConfig.KnightConfigs[2].Lives)
        {
            for (int i = 1; i <= 2; i++)
            {
                lives[lives.Count - i].gameObject.SetActive(true);
            }

            FillSpell(knightsConfig.KnightConfigs[2].Lives);
        }
    }
    public void ReFillSpell()
    {
        for (int i = 0; i < nameSpell.Count; i++)
        {
            lives[i].sprite = live[Random.Range(0, 3)];

            if (lives[i].sprite == live[0])
            {
                nameSpell[i] = Ids.DrowLinePoint;
            }
            if (lives[i].sprite == live[1])
            {
                nameSpell[i] = Ids.DrowLineHorizontal;
            }
            if (lives[i].sprite == live[2])
            {
                nameSpell[i] = Ids.DrowLineVertical;
            }
        }
    }
    public void FillSpell(int _lives)
    {
        if(nameSpell.Count < _lives)
        {
            nameSpell.RemoveRange(0, nameSpell.Count);
            for (int i = 0; i < _lives; i++)
            {
                lives[i].sprite = live[Random.Range(0, 3)];

                if (lives[i].sprite == live[0])
                {
                    nameSpell.Add(Ids.DrowLinePoint);
                }
                if (lives[i].sprite == live[1])
                {
                    nameSpell.Add(Ids.DrowLineHorizontal);
                }
                if (lives[i].sprite == live[2])
                {
                    nameSpell.Add(Ids.DrowLineVertical);
                }
            }
        } 
    }
    void RestoreHp()
    {
        for(int i = 0; i < nameSpell.Count; i++) 
        {
            if (lives[i].sprite == emptyLive[0])
            {
                lives[i].sprite = live[0];
            }
            if (lives[i].sprite == emptyLive[1])
            {
                lives[i].sprite = live[1];
            }
            if (lives[i].sprite == emptyLive[2])
            {
                lives[i].sprite = live[2];
            }
        }   
    }
    public void TakeDamage(string nameDrowSpell)
    {
        for(int i = 0; i < nameSpell.Count; i++)
        {
            if (iterator == i && nameSpell[i] == nameDrowSpell && curr == false)
            {
                if (lives[i].sprite == live[0])
                {
                    lives[i].sprite = emptyLive[0];
                }
                if (lives[i].sprite == live[1])
                {
                    lives[i].sprite = emptyLive[1];
                }
                if (lives[i].sprite == live[2])
                {
                    lives[i].sprite = emptyLive[2];
                }
                curr = true;
                iterator++; 
            }

            if (nameDrowSpell == Ids.DrowLineNoLine)
            {
                curr = false;
            }

            if(iterator == i && nameSpell[i] != nameDrowSpell && curr == false)
            {
                if(nameDrowSpell != Ids.DrowLineNoLine)
                {
                    RestoreHp();
                    iterator = 0;
                }
            }
        }      
    }
}
