﻿using Drow.Core;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Drow
{
    public class GameDirector : SceneDirector
    {
        protected override void Start()
        {
            base.Start();

            SetCurrentScreen<GameScreen>().ShowAndStartGame();
        }
        protected override void OnScreenExit(Type _screenType, string _exitCode)
        {
            if(_screenType == typeof(GameScreen))
            {
                if(_exitCode == GameScreen.Exit_Pause)
                {
                    SetCurrentScreen<PauseScreen>().Show();
                }
                else if(_exitCode == GameScreen.Exit_Result)
                {
                    SetCurrentScreen<ResultScreen>().Show();
                }
            }
            else if (_screenType == typeof(PauseScreen))
            {
                if (_exitCode == PauseScreen.Exit_Back)
                {
                    Time.timeScale = 1;
                    ToBackScreen();
                }
                else if(_exitCode == PauseScreen.Exit_Menu)
                {
                    SceneManager.LoadScene(ScenesIds.Menu);
                }
            }
            else if(_screenType == typeof(ResultScreen))
            {
                if (_exitCode == ResultScreen.Exit_MenuResult)
                {
                    SceneManager.LoadScene(ScenesIds.Menu);
                }
            }
        }
    }
}

