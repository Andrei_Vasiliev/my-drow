﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow
{
    public class SkyMover : MonoBehaviour
    {
        [SerializeField]
        List<Cloud> tiles;

        [SerializeField]
        float speed;

        private void Update()
        {
            if (isMovie)
            {
                for(int i = 0; i < tiles.Count; i++)
                {
                    var _tile = tiles[i];

                    _tile.Position -= speed * Time.deltaTime;
                    if(_tile.Position < -_tile.Length)
                    {
                        var _lastTile = tiles[tiles.Count - 1];

                        _tile.Position = _lastTile.Position + _lastTile.Length;

                        tiles.RemoveAt(i);
                        tiles.Add(_tile);

                        i--;
                    }
                }
            }
        }
        public bool isMovie { get; set; }
    }
}

