﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow
{
    public class Tutorial : MonoBehaviour
    {
        public static float StartTime = 0;
        [SerializeField]
        float EndTime;

        private void Update()
        {
            if(gameObject.activeSelf == true)
            {

                StartTime += 0.1f * Time.deltaTime;

                if (StartTime >= EndTime)
                {
                    StartTime = 0;
                    gameObject.SetActive(false);
                }
            }
        }
    }
}

