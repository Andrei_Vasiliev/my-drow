﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow
{
    public class Cloud : MonoBehaviour
    {
        [SerializeField]
        float length;

        public float Position
        {
            get => transform.localPosition.x;

            set
            {
                var _pos = transform.localPosition;
                _pos.x = value;
                transform.localPosition = _pos;
            }
        }
        public float Length => length;
    }
}

