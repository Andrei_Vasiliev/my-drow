﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow
{
    public class DrawLine : MonoBehaviour
    {
        [SerializeField]
        GameObject linePrefab;
        GameObject currentLine;

        [SerializeField]
        GameObject trackPrefab;

        LineRenderer lineRenderer;
        EdgeCollider2D edgeCollider;
        List<Vector2> fingerPositions;

        Vector2 temp = Vector2.zero;
        Vector2 newTemp = Vector2.zero;
        Vector2 diff;

        List<Vector2> newTempList;
        public static string NameLine = Ids.DrowLineNoLine;

        [SerializeField]
        Transform parentGame;

        Camera gameCamera;

        void Awake()
        {
            gameCamera = GameObject.Find("CameraGame").GetComponent<Camera>();
            fingerPositions = new List<Vector2>();
            newTempList = new List<Vector2>();
        }
        private void Update()
        {
            if (Track && Input.GetMouseButtonUp(0))
            {
                Destroy(Track, 0.2f);
            }
            if (Input.GetMouseButtonDown(0))
            {
                temp = Vector2.zero;
                CreateLine();
                newTempList.Clear();
                diff = Vector2.zero;
                NameLine = Ids.DrowLineNoLine; 
            }
            if (Input.GetMouseButton(0))
            {
                Vector2 tempFingerPos = gameCamera.ScreenToWorldPoint(Input.mousePosition);
                if (Vector2.Distance(tempFingerPos, fingerPositions[fingerPositions.Count - 1]) > .1f)
                {
                    UpdateLine(tempFingerPos);
                    for (int i = 0; i < fingerPositions.Count; i++)
                    {
                        if (i > 0)
                        {
                            temp += fingerPositions[i] - fingerPositions[i - 1];
                            newTemp = fingerPositions[i] - fingerPositions[i - 1];
                            newTempList.Add(newTemp);
                        }
                    }
                }
            }
            if (Input.GetMouseButtonUp(0))
            {
                EqualsLines();
                Destroy(currentLine);
            }
        }
        void CreateLine()
        {
            currentLine = Instantiate(linePrefab, Vector3.zero, Quaternion.identity, parentGame);
            lineRenderer = currentLine.GetComponent<LineRenderer>();
            edgeCollider = currentLine.GetComponent<EdgeCollider2D>();
            fingerPositions.Clear();
            fingerPositions.Add(gameCamera.ScreenToWorldPoint(Input.mousePosition));
            fingerPositions.Add(gameCamera.ScreenToWorldPoint(Input.mousePosition));
            lineRenderer.SetPosition(0, fingerPositions[0]);
            lineRenderer.SetPosition(1, fingerPositions[1]);
            edgeCollider.points = fingerPositions.ToArray();

            Track = Instantiate(trackPrefab, fingerPositions[0], Quaternion.identity, parentGame);
            Track.transform.position = fingerPositions[0];
        }
        void UpdateLine(Vector2 newFingerPos)
        {
            fingerPositions.Add(newFingerPos);
            lineRenderer.positionCount++;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, newFingerPos);
            edgeCollider.points = fingerPositions.ToArray();

            Track.transform.position = newFingerPos;
        }

        void EqualsLines()
        {
            if (temp == Vector2.zero)
            {
                NameLine = Ids.DrowLinePoint;
            }

            if (Mathf.Abs(temp.y) > Mathf.Abs(temp.x))
            {
                for (int i = 0; i < newTempList.Count; i++)
                {
                    if (i > 0)
                    {
                        diff = newTempList[i] - newTempList[i - 1];
                        if (diff.x > 0.06f)
                        {
                            return;
                        }
                    }
                }
                NameLine = Ids.DrowLineVertical;
            }

            if (Mathf.Abs(temp.x) > Mathf.Abs(temp.y))
            {
                for (int i = 0; i < newTempList.Count; i++)
                {
                    if (i > 0)
                    {
                        diff = newTempList[i] - newTempList[i - 1];
                        if (diff.y > 0.06f)
                        {
                            return;
                        }
                    }
                }
                NameLine = Ids.DrowLineHorizontal;
            }
        }
        public static GameObject Track { get; set; }
    }
}

