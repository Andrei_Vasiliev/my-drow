﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public class AllWinState : BaseState
    {
        [SerializeField]
        GameObject knight;

        Animator knightAnimator;

        private void Awake()
        {
            knightAnimator = knight.GetComponent<Animator>();
        }

        private void Update()
        {
            knightAnimator.SetBool(Ids.KnightAnimatorAllWin, true);
        }
        public override KnightState KnightState => KnightState.AllWin;
    }
}

