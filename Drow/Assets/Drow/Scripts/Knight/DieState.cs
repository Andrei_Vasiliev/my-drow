﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow.Knight
{
    public class DieState : BaseState
    {
        Animator knightAnimator;
        Animator wizardAnimator;
        SpellSystem reboot;

        [SerializeField]
        GameObject lightPrefab;

        [SerializeField]
        GameObject spellPrefab;

        [SerializeField]
        Vector3 direction;

        [SerializeField]
        GameObject knight;

        [SerializeField]
        AudioClip witherSound;

        bool play = true;
        int iterator;

        private void Awake()
        {
            iterator = spellPrefab.GetComponent<SpellSystem>().iterator;
            reboot = spellPrefab.GetComponent<SpellSystem>();
            knightAnimator = knight.GetComponent<Animator>();
            wizardAnimator = GameObject.Find("Wizard").GetComponent<Animator>();
        }

        private void FixedUpdate()
        {
            knight.transform.Translate(direction);
        }
        void Update()
        {
            if(play == true)
            {
                play = false;
                SoundManager.Instance.PlaySound(witherSound);
            }

            if(wizardAnimator.GetInteger(Ids.WizardAnimator) == 1) 
            {
                Invoke("OnCastSpell", 0.5f);
            }

            lightPrefab.gameObject.SetActive(true);
            direction = Vector2.zero;
            knightAnimator.SetBool(Ids.KnightAnimatorDie, true);
            Invoke("OnDestroy", 1.6f);
        }

        void OnCastSpell()
        {
            wizardAnimator.SetInteger(Ids.WizardAnimator, 0);
        }
        private void OnDestroy()
        {
            play = true;
            iterator = 0;

            reboot.ReFillSpell();

            gameObject.SetActive(false);
            knight.SetActive(false); 
        }
        public override KnightState KnightState => KnightState.Die;
    }
}


