﻿using Drow.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public class RunState : BaseState
    {
        public override KnightState KnightState => KnightState.Run;

        [SerializeField]
        GameObject spellPrefab;

        [SerializeField]
        Vector3 direction;

        [SerializeField]
        GameObject knight;

        [SerializeField]
        Canvas canSpell;

        [SerializeField]
        AudioClip[] wizardCastSounds;

        Animator wizardAnimator;
        SpellSystem spellSystem;

        private void Awake()
        {
            spellSystem = spellPrefab.GetComponent<SpellSystem>();
            wizardAnimator = GameObject.Find("Wizard").GetComponent<Animator>();
            canSpell.worldCamera = GameObject.Find("CameraGame").GetComponent<Camera>();
        }
        private void FixedUpdate()
        {
            knight.transform.Translate(direction);
        }

        private void Update()
        {
            spellSystem.TakeDamage(DrawLine.NameLine);

            if (spellSystem.iterator == spellSystem.nameSpell.Count)
            {
                spellSystem.iterator = 0;
                GameInfo.Instance.RegisterScores();
                wizardAnimator.SetInteger(Ids.WizardAnimator, 1);
                SoundManager.Instance.PlaySound(wizardCastSounds[Random.Range(0,wizardCastSounds.Length)]);
                
                NextStateAction.Invoke(KnightState.Die);
            }

            if(knight.transform.position.x < -3.8f)
            {
                wizardAnimator.SetInteger(Ids.WizardAnimator, 2);
                NextStateAction.Invoke(KnightState.Win);
            }

            if(wizardAnimator.GetInteger(Ids.WizardAnimator) == 2 && knight.transform.position.x > -3.8f)
            {
                NextStateAction.Invoke(KnightState.AllWin);
            }
        }
    }
}