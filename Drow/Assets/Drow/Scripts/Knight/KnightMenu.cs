﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public class KnightMenu : MonoBehaviour
    {
        [SerializeField]
        Vector2 direction;
        [SerializeField]
        SpriteRenderer sr;

        private void FixedUpdate()
        {
            transform.Translate(direction);
            if(transform.position.x < -6.5f)
            {
                direction = -direction;
                sr.flipX = true;
            }
            else if(transform.position.x > 6.5f)
            {
                direction = -direction;
                sr.flipX = false;
            }
        }
    }
}