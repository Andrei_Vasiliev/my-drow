﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public enum KnightState
    {
        Run,
        Die,
        Win,
        AllWin
    }
    public class KnightController_2 : MonoBehaviour
    {
        List<BaseState> states;
        BaseState currentState;

        private void Awake()
        {
            states = new List<BaseState>(transform.GetComponentsInChildren<BaseState>(true));

            states.ForEach(_state =>
            {
                _state.NextStateAction = OnNextStateRequest;
            });

            currentState = states.Find(_state => _state.KnightState == KnightState.Run);
            currentState.Activate();
        }

        void OnNextStateRequest(KnightState _state)
        {
            currentState.Deactivate();
            currentState = states.Find(_s => _s.KnightState == _state);
            currentState.Activate();
        }
        public void Prepare(Vector3 _position, Quaternion _rotation)
        {
            transform.position = _position;
            transform.rotation = _rotation;
            gameObject.SetActive(true);

            states.ForEach(_state =>
            {
                _state.NextStateAction = OnNextStateRequest;
            });

            states.Find(_state => _state.KnightState == KnightState.Die).Deactivate();
            currentState = states.Find(_state => _state.KnightState == KnightState.Run);
            currentState.Activate();
        }
        public bool IsActive => gameObject.activeSelf;
    }
}

