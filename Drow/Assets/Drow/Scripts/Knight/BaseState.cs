﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public abstract class BaseState : MonoBehaviour
    { 
        public abstract KnightState KnightState { get; }

        public virtual void Activate()
        {
            gameObject.SetActive(true);
        }

        public virtual void Deactivate()
        {
            gameObject.SetActive(false);
        }

        public Action<KnightState> NextStateAction { get; set; }
    }
}

