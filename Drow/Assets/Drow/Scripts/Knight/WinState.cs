﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Drow.Knight
{
    public class WinState : BaseState
    {
        [SerializeField]
        Animator knightAnimator;
        [SerializeField]
        AudioClip wizardGameOver;
        private void Start()
        {
            knightAnimator.SetBool(Ids.KnightAnimatorWin, true);
            SoundManager.Instance.PlaySound(wizardGameOver);
        }
        public override KnightState KnightState => KnightState.Win;
    }
}

