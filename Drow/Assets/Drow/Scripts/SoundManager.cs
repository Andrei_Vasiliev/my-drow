﻿using Drow.Base;
using Drow.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Drow
{
    public class SoundManager : BaseManager<SoundManager>
    {
        const float MUTE_VOLUME = -80;
        [SerializeField]
        AudioMixer mixer;

        [SerializeField]
        AudioClip musicSoundGame;

        [SerializeField]
        AudioClip musicSoundMenu;

        [SerializeField]
        AudioClip buttonSound; 

        [SerializeField]
        AudioSource musicSource;

        [SerializeField]
        AudioSource soundSource;

        public void PlayMusicGame()
        {
            musicSource.clip = musicSoundGame;
            musicSource.Play();
        }

        public void PlayMusicMenu()
        {
            musicSource.clip = musicSoundMenu;
            musicSource.Play();
        }
        public void PlayButtonSound()
        {
            soundSource.PlayOneShot(buttonSound);
        }

        public void PlaySound(AudioClip _sound)
        {
            soundSource.PlayOneShot(_sound); 
        }

        public void MuteMusic(bool _value)
        {
            GameInfo.Instance.userData.IsMusicOn = !_value;
            mixer.SetFloat("MusicVolume", _value ? MUTE_VOLUME : 0);
        }

        public void MuteSounds(bool _value)
        {
            GameInfo.Instance.userData.IsSoundOn = !_value;
            mixer.SetFloat("SoundsVolume", _value ? MUTE_VOLUME : 0);
        }

        public bool SoundState => GameInfo.Instance.userData.IsSoundOn;
        public bool MusicState => GameInfo.Instance.userData.IsMusicOn;
    }
}