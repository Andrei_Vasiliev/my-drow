﻿using Drow.Base;
using Drow.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Drow
{
    public class PauseScreen : BaseScreen
    {
        public const string Exit_Back = "Exit_Back";
        public const string Exit_Menu = "Exit_Menu";

        [SerializeField]
        Toggle soundTougle;
        [SerializeField]
        Toggle musicTougle;

        public override void Show()
        {
            base.Show();

            soundTougle.isOn = SoundManager.Instance.SoundState;
            musicTougle.isOn = SoundManager.Instance.MusicState;
        }
        public void OnBackPressed()
        {
            Exit(Exit_Back);
            SoundManager.Instance.PlayButtonSound();
        }
        public void OnMenuPressed()
        {
            Time.timeScale = 1;
            SoundManager.Instance.PlayButtonSound();
            Exit(Exit_Menu);
        }

        public void OnMusicToggle(bool _value)
        {
            SoundManager.Instance.MuteMusic(!_value);
            AppPrefs.SetObject(Ids.UserData, GameInfo.Instance.userData);
            AppPrefs.Save();
        }

        public void OnSoundsToggle(bool _value)
        {
            SoundManager.Instance.MuteSounds(!_value);
            AppPrefs.SetObject(Ids.UserData, GameInfo.Instance.userData);
            AppPrefs.Save();
        }
    }
}
