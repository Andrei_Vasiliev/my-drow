﻿using Drow.Base;
using Drow.Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Drow
{
    public class MenuScreen : BaseScreen
    {
        [SerializeField]
        SkyMover skyMover;

        public const string Exit_Game = "Exit_Game";

        [SerializeField]
        TextMeshProUGUI scoreText;
        public override void Show()
        {
            base.Show();

            GameInfo.Instance.Setup();
            SoundManager.Instance.PlayMusicMenu();
            scoreText.text = GameInfo.Instance.BestScores.ToString();
            skyMover.isMovie = true;
        }
        public void OnGamePressed()
        {
            Time.timeScale = 1;
            SoundManager.Instance.PlayButtonSound();
            Exit(Exit_Game);
        }
    }
}