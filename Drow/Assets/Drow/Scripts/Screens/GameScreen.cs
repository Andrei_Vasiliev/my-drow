﻿using Drow.Base;
using Drow.Core;
using Drow.Knight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Drow
{
    public class GameScreen : BaseScreen
    {
        public const string Exit_Pause = "Exit_Pause";
        public const string Exit_Result = "Exit_Result";

        [SerializeField]
        SkyMover skyMover;
        [SerializeField]
        ScoreCounter scoreCounter;
        [SerializeField]
        Animator wizardAnimator;
        [SerializeField]
        GameObject tutorial;

        public void ShowAndStartGame()
        {
            Show();
            GameInfo.Instance.Setup();

            SoundManager.Instance.PlayMusicGame();

            skyMover.isMovie = true;
            GameInfo.Instance.InGameScores = 0;
        }
        private void Update()
        {
            if (wizardAnimator.GetInteger(Ids.WizardAnimator) == 2)
            {
                GameInfo.Instance.RegisterResult(GameInfo.Instance.BestScores);
                Exit(Exit_Result);
            }

            scoreCounter.SetScores(GameInfo.Instance.InGameScores);
        }
        public void OnPausePressed()
        {
            Destroy(DrawLine.Track);
            Time.timeScale = 0;
            GameInfo.Instance.RegisterResult(GameInfo.Instance.BestScores);
            SoundManager.Instance.PlayButtonSound();
            Exit(Exit_Pause);   
        }

        public void OnTutorialPressed()
        {
            if(tutorial.activeSelf == false)
            {
                tutorial.SetActive(true);
                Time.timeScale = 0;
            }
            else if(tutorial.activeSelf == true)
            {
                Tutorial.StartTime = 0;
                Time.timeScale = 1;
                tutorial.SetActive(false);
            }
            
        }
    }
}

