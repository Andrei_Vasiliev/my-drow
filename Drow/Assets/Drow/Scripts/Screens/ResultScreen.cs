﻿using Drow.Base;
using Drow.Core;
using Drow.Knight;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Drow
{
    public class ResultScreen : BaseScreen
    {
        [SerializeField]
        TextMeshProUGUI score;

        public const string Exit_MenuResult = "Exit_MenuResult";
        public override void Show()
        {
            base.Show();

            GameInfo.Instance.RegisterResult(GameInfo.Instance.BestScores);
            score.text = GameInfo.Instance.InGameScores.ToString();
        }
        public void OnMenuResultPressed()
        {
            SoundManager.Instance.PlayButtonSound();

            Exit(Exit_MenuResult);
        }
    }
}

