﻿using Drow.Knight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow
{
    public class EnemyPool : MonoBehaviour
    {
        [SerializeField]
        GameObject knightPrefab;

        List<KnightController_2> knights;
        private void Awake()
        {
            knights = new List<KnightController_2>();

            var _knights = transform.GetComponentsInChildren<KnightController_2>(true);
            knights.AddRange(_knights);
        }
        public KnightController_2 GetKnight()
        {
            KnightController_2 _knight = knights.Find(_k => !_k.IsActive);

            if(_knight == null)
            {
                _knight = Instantiate(knightPrefab, transform).GetComponent<KnightController_2>();
                knights.Add(_knight);
            }

            return _knight;
        }
    }
}

