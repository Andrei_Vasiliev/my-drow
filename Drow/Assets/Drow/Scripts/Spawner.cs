﻿using Drow.Knight;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow
{
    public class Spawner : MonoBehaviour
    {
        [SerializeField]
        EnemyPool enemyPool;

        [SerializeField]
        Transform knightPointTr;

        [SerializeField]
        private float spawnRate = 2f;
        float nextSpawn = 0.0f;

        [SerializeField]
        Animator wizardAnimator;

        private void Update()
        {
            if(wizardAnimator.GetInteger(Ids.WizardAnimator) != 2)
            {
                if (Time.time > nextSpawn)
                {
                    nextSpawn = Time.time + spawnRate;
                    var _knight = enemyPool.GetKnight();

                    _knight.Prepare(knightPointTr.position, knightPointTr.rotation);
                }
            }
        }
    }
}

