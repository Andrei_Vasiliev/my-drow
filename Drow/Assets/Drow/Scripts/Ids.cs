﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Drow
{
    public class Ids : MonoBehaviour
    {
        public const string WizardAnimator = "wizard";

        public const string KnightAnimatorAllWin = "all_win";
        public const string KnightAnimatorWin = "win";
        public const string KnightAnimatorDie = "die";

        public const string DrowLineNoLine = "not line";
        public const string DrowLinePoint = "point";
        public const string DrowLineVertical = "vertical";
        public const string DrowLineHorizontal = "horizontal";

        public const int SCORES_FOR_KIGHT = 10;

        public const string UserData = "UserData";

    }
}

