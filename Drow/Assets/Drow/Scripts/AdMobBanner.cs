﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdMobBanner : MonoBehaviour
{
    private BannerView bannerView;

    private const string bannerUnitId = "ca-app-pub-6308599143263139/3179786126";

    private void OnEnable()
    {
        bannerView = new BannerView(bannerUnitId, AdSize.Banner, AdPosition.Bottom);
        AdRequest adRequest = new AdRequest.Builder().Build();
        bannerView.LoadAd(adRequest);
        StartCoroutine(ShowBanner());

        IEnumerator ShowBanner()
        {
            yield return new WaitForSecondsRealtime(3.0f);
            bannerView.Show();
        }
    }
}
