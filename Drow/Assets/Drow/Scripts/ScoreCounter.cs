﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Drow
{
    public class ScoreCounter : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI score;

        public void SetScores(int _scores)
        {
            score.text = _scores.ToString();
        }
    }
}