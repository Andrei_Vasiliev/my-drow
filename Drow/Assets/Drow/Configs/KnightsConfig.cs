﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Drow
{
    [CreateAssetMenu]
    public class KnightsConfig : ScriptableObject
    {
        [SerializeField]
        List<KnightConfig> knightsConfigs;

        public List<KnightConfig> KnightConfigs => knightsConfigs;
    }

    [Serializable]
    public class KnightConfig
    {
        [SerializeField]
        int lives;

        public int Lives { get => lives; set => lives = value; }
    }
}